package clases;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import excepciones.MazoMuyLargoException;

public class Usuario {
	/*
	 * @param String nombre coge el nombre del usuario
	 * @param String alias coge el alias del usuario
	 * @param String contraseña coge la contraseña del usuario
	 * @param String email coge el email del usuario
	 * @param Connection bd realiza la conexion del usuario a la base de datos
	 * @param int hp coge la vida del usuario
	 * @param HashSet<Carta>mazo hace un hashSet de tipo carta que va a ser el mazo
	 * @param HashMap<String,Carta> mano hace un hashMap que va a ser la mano
	 * @param ArrayList<Carta>tablero hace un arraylist de tipo carta que sera el tablero
	 */
private String nombre;
private String alias;
private String contraseña;
private String email;
private Connection bd;
private int hp;
private BufferedImage heroe;

/*HashSet de mazo
 * HashMap de mano
 * ArrayList de tablero
 * ArrayList de tableroFuturo
 */
private HashSet<Carta>mazo;
private HashMap<String,Carta> mano;
private ArrayList<Carta>tablero;
private ArrayList<Carta> tableroFuturo;
private Ventanas ventana;
//Constructor de Usuario
public Usuario(String nombre, String alias, String contraseña, String email) {
	super();
	this.nombre = nombre;
	this.alias = alias;
	this.contraseña = contraseña;
	this.email = email;
	
	mazo=new HashSet<Carta>();
	mano=new HashMap<String,Carta>();

}
//Constructor de Usuario para la pelea
public Usuario (String nombre, BufferedImage heroe, int hp) {
	this.nombre=nombre;
	this.heroe=heroe;
	this.hp=30;
	this.tableroFuturo=new ArrayList<Carta>();
	mazo=new HashSet<Carta>();
	mano=new HashMap<String,Carta>();
}

public ArrayList<Carta> getTableroFuturo() {
	return tableroFuturo;
}
public void setTableroFuturo(ArrayList<Carta> tableroFuturo) {
	this.tableroFuturo = tableroFuturo;
}
//Usuario recoge el arrayList de tableroFuturo, el hashSet de mazo y el hashMap de mano
public Usuario (String nombre) {
	this.tableroFuturo=new ArrayList<Carta>();
	this.nombre=nombre;
	mazo=new HashSet<Carta>();
	mano=new HashMap<String,Carta>();
}

public int getHp() {
	return hp;
}
//Excepcion propia
public void añadirAlMazo(Carta c) throws MazoMuyLargoException {
	if(this.mazo.size()>=30) {
		throw new MazoMuyLargoException("No puedes introducir mas de 30 cartas");
	}
	mazo.add(c);
}
/*Getter y setter de:
 * BufferedImage
 * hp
 * mazo
 * mano
 * tablero
 * nombre
 * alias
 * contraseña
 * email
 */
public BufferedImage getHeroe() {
	return heroe;
}
public void setHeroe(BufferedImage heroe) {
	this.heroe = heroe;
}
public void setHp(int hp) {
	this.hp = hp;
}
public HashSet<Carta> getMazo() {
	return mazo;
}
public void setMazo(HashSet<Carta> mazo) {
	this.mazo = mazo;
}
public HashMap<String,Carta> getMano() {
	return mano;
}
public void setMano(HashMap<String,Carta> mano) {
	this.mano = mano;
}
public ArrayList<Carta> getTablero() {
	return tablero;
}
public void setTablero(ArrayList<Carta> tablero) {
	this.tablero = tablero;
}
public Usuario (String alias,String contraseña) {
	super();
	this.alias=alias;
	this.contraseña=contraseña;
	mazo=new HashSet<Carta>();
	mano=new HashMap<String,Carta>();
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getAlias() {
	return alias;
}
public void setAlias(String alias) {
	this.alias = alias;
}
public String getContraseña() {
	return contraseña;
}
public void setContraseña(String contraseña) {
	this.contraseña = contraseña;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}

}

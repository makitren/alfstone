package clases;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import interfaces.CreacionMazo;
import interfaces.Lobby;
import interfaces.PantallaInicio;
import interfaces.PantallaJefes;
import interfaces.PantallaLogin;
import interfaces.PantallaRegistro;
import interfaces.TableroJuego;

public class Ventanas extends JFrame{
private PantallaInicio ptInicio;
private CreacionMazo ptCreacion;
private PantallaLogin ptLogin;
private PantallaRegistro ptRegistro;
private Usuario usuarioLog;
private Enemigo enemigo;
private Ventanas ventana;
private Lobby ptLobby;
private ArrayList<Carta>cartasPosibles;
private PantallaJefes ptJefes;
private TableroJuego ptTablero;
private HashMap<String,Carta> mano;
/* @param PantallaInicio ptInicio se encarga de entrar en la pantalla de inicio
 * @param CreacionMazo ptCreacion se encarga de entrar en la pantalla de Creacion
 * @param PantallaLogin ptLogin se encarga de entrar en la pantalla de Login
 * @param PantallaRegistro ptRegistro se encarga de entrar en la pantalla de Registro
 * @param Usuario usuarioLog  se encarga de entrar en la clase de Usuario a pedir la informacion
 * @param Enemigo enemigo permite entrar en la clase Enemigo a pedir la informacion
 * @param Lobby ptLobby se encarga de entrar en la pantalla de lobby
 * @param PantallaJefes ptJefes se encarga de entrar en la pantalla de jefes
 * @param TableroJuego ptTablero se encarga de entrar en la pantalla de tablero
 * 
 */
public Enemigo getEnemigo() {
	return enemigo;

}
public void setEnemigo(Enemigo enemigo) {
	this.enemigo = enemigo;
}

public Ventanas(ArrayList<Carta>carta) {
	this.cartasPosibles=carta;
		this.setSize(1200,1000);
	this.setTitle("Alfstone");
	this.ptInicio=new PantallaInicio(this);
	this.setContentPane(ptInicio);
	this.setVisible(true);
}
public Usuario getUsuarioLog() {
	return usuarioLog;
}
public TableroJuego getTableroJuego() {
	return ptTablero;
}
public void setUsuarioLog(Usuario usuarioLog) {
	this.usuarioLog=usuarioLog;
}
//Entra en la pantalla de Inicio de sesion
public void cargarPantallaInicioSesion() {
if(ptLogin==null) {
	this.ptLogin=new PantallaLogin(this);
}
this.setTitle("Inicio Sesion");
this.ptInicio.setVisible(false);
this.setSize(1200, 1000);
this.setContentPane(ptLogin);
this.ptLogin.setVisible(true);
}
//Entra en la pantalla de los jefes
public void cargarPantallaJefes() {
if(ptJefes==null) {
	this.ptJefes=new PantallaJefes(this);
	
}
this.setTitle("PantallaJefes");
this.ptLobby.setVisible(false);
this.setSize(1200, 1000);
this.setContentPane(ptJefes);
this.ptJefes.setVisible(true);
}
//Entra en la pantalla del tablero
public void irATablero() {
	if(usuarioLog.getMazo().size()==30) {
		if(ptTablero==null) {
			this.ptTablero=new TableroJuego(this,enemigo);
		}
		this.setTitle("Tablero");
		this.ptJefes.setVisible(false);
		this.setContentPane(ptTablero);
		this.setSize(1700,1020);
		this.ptTablero.setVisible(true);
	}else {
		JOptionPane.showMessageDialog(ventana, "Tu mazo no tiene 30 cartas, no puedes luchar aun");
	}
}
//Entra en la pantalla del registro de usuario
public void cargarPantallaRegistroUsuario() {
	if(ptRegistro==null) {
		this.ptRegistro=new PantallaRegistro(this);
	}
	this.setTitle("Registrar Usuario");
	this.ptInicio.setVisible(false);
	this.setSize(1200, 1000);
	this.setContentPane(ptRegistro);
	this.ptRegistro.setVisible(true);
}
//Entra en el vestibulo
public void cargarPantallaLobby() {
	if(this.ptLobby==null) {
		this.ptLobby=new Lobby(this);
	}
	if(this.ptRegistro!=null) {
		this.ptRegistro.setVisible(false);
	}
	if(this.ptCreacion!=null) {
		this.ptCreacion.setVisible(false);
	}
	if(this.ptJefes!=null) {
		this.ptJefes.setVisible(false);
	}
	if(this.ptLogin!=null) {
		this.ptLogin.setVisible(false);
	}
	
	this.setTitle("VestÝbulo");
	this.setSize(1200,1000);
	this.setContentPane(ptLobby);
	this.ptLobby.setVisible(true);
}
//Entra en la pantalla de la creacion del mazo	
public void cargarPantallaCreacionMazo() {
	if(ptCreacion==null) {
		this.ptCreacion=new CreacionMazo(this,this.cartasPosibles);
	}
	if(ptLobby!=null) {
		this.ptLobby.setVisible(false);
	}
		this.setTitle("Creacion de mazo");
		
		this.setSize(1200, 1000);
		this.setContentPane(ptCreacion);
		this.ptCreacion.setVisible(true);
	
}
//carga la primera pantalla

public void cargarPantallaInicio() {
	
	if (this.ptLogin!=null) {
		this.ptLogin.setVisible(false);
	} else if (this.ptRegistro!=null) {
		this.ptRegistro.setVisible(false);
	}else if(this.ptLobby!=null) {
		this.ptLobby.setVisible(false);
	}
	this.setSize(1200,1000);
	this.setTitle("Alfstone");
	this.ptInicio=new PantallaInicio(this);
	this.setContentPane(ptInicio);
	this.ptInicio.setVisible(true);
}



}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;

/**
 *
 * @author 1DAM
 */
public class Carta extends JButton{
    /*
     * @param int id devuelve entero de id
     * @param int mana devuelve el coste de la carta
     * @param int atk ataque de la carta
     * @param int hp vida de la carta
     * @param String name devuelve nombre de la carta
     * @param String description devuelve descripcion de la carta
     * @param TipoCarta tipoCarta declara el tipo de la carta, puede ser NORMAL, PROVOCAR Y SIGILO.
     * @param BufferedImage imagen devuelve la imagen de la carta
     */
	private int id;
	private int mana;
    private int atk;
    private int hp;
    private String name;
    private String description;
    private TipoCarta tipoCarta;
    private BufferedImage imagen;
    
    
    /*
     * switch nombre se encarga de introducir la carta
     */
    public Carta(String nombre,int hp) {
    	switch(nombre) {
    	case "Rotulillo":
    		id=1;
    		atk=1;
    		this.hp=hp;
    		name=nombre;
    		description="Le gustan las rotulas";
    		tipoCarta=TipoCarta.NORMAL;
    		try {
				imagen=ImageIO.read(new File("src/imagenes1/RotulilloBueno.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		mana=1;
    		break;
    	case "Mosquito Cenec":
    		id=2;
    		atk=2;
    		this.hp=hp;
    		name=nombre;
    		description="Te zumba la oreja";
    		tipoCarta=TipoCarta.NORMAL;
    		try {
				imagen=ImageIO.read(new File("src/imagenes1/MosquitoCenecBueno.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		mana=2;
    		break;
    	case "Alberto":
    		id=3;
    		atk=3;
    		this.hp=hp;
    		name=nombre;
    		description="Viene,golpea y se va";
    		tipoCarta=TipoCarta.NORMAL;
    		try {
				imagen=ImageIO.read(new File("src/imagenes1/AlbertoBueno.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		mana=3;
    		break;
    	case "Silla torcida":
    		id=4;
    		atk=2;
    		this.hp=hp;
    		name=nombre;
    		description="Te incomoda";
    		tipoCarta=TipoCarta.NORMAL;
    		try {
				imagen=ImageIO.read(new File("src/imagenes1/SillaTorcidaBueno.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		mana=4;
    		break;
    	case "Teclado sucio":
    		id=5;
    		atk=5;
    		this.hp=hp;
    		name=nombre;
    		description="Mugre en los dedos";
    		tipoCarta=TipoCarta.NORMAL;
    		try {
				imagen=ImageIO.read(new File("src/imagenes1/TecladoSucioBueno.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		mana=5;
    		break;
    	case "Murloc enfadado":
    		id=6;
    		atk=7;
    		this.hp=hp;
    		name=nombre;
    		description="A toda ostia";
    		tipoCarta=TipoCarta.NORMAL;
    		try {
				imagen=ImageIO.read(new File("src/imagenes1/MurlocEnfadadoBueno.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		mana=6;
    		
    		break;
    		case "Patricio":
        		id=7;
        		atk=7;
        		this.hp=hp;
        		name=nombre;
        		description="Que raios";
        		tipoCarta=TipoCarta.NORMAL;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/PatricioBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=7;
        		break;
    		case "Pingu":
        		id=8;
        		atk=14;
        		this.hp=hp;
        		name=nombre;
        		description="Un pe�etaso";
        		tipoCarta=TipoCarta.NORMAL;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/PinguBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=8;
        		break;
    		case "Paloma N.1":
        		id=9;
        		atk=8;
        		this.hp=hp;
        		name=nombre;
        		description="Una paloma de N.";
        		tipoCarta=TipoCarta.NORMAL;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/PalomaBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=9;
        		break;
    		case "Gracian":
        		id=10;
        		atk=12;
        		this.hp=hp;
        		name=nombre;
        		description="Te da bien fuerte";
        		tipoCarta=TipoCarta.NORMAL;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/GracianBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=10;
        		break;
    		case "Un yonki":
        		id=11;
        		atk=5;
        		this.hp=hp;
        		name=nombre;
        		description="Te navajea";
        		tipoCarta=TipoCarta.NORMAL;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/YonkiBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=3;
        		break;
    		case "Un pedrolo":
        		id=12;
        		atk=2;
        		this.hp=hp;
        		name=nombre;
        		description="Te zumba la oreja";
        		tipoCarta=TipoCarta.NORMAL;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/PedroloBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=5;
        		break;
    		case "Antonio":
        		id=13;
        		atk=9;
        		this.hp=hp;
        		name=nombre;
        		description="Te insulta";
        		tipoCarta=TipoCarta.NORMAL;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/AntonioBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=7;
        		break;
    		case "Cangrechillo":
        		id=14;
        		atk=1;
        		this.hp=hp;
        		name=nombre;
        		description="Cuidao";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/CangrechilloBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=1;
        		break;
    		case "Una brocheta":
        		id=15;
        		atk=1;
        		this.hp=hp;
        		name=nombre;
        		description="Pum colesterol";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/BrochetaBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=2;
        		break;
    		case "Sheldon":
        		id=16;
        		atk=2;
        		this.hp=hp;
        		name=nombre;
        		description="Toc Toc";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/SheldonBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=3;
        		break;
    		case "Walter":
        		id=17;
        		atk=2;
        		this.hp=hp;
        		name=nombre;
        		description="Te tira meta";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/WalterBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=4;
        		break;
    		case "La cosa":
        		id=18;
        		atk=3;
        		this.hp=hp;
        		name=nombre;
        		description="Se nos va de las manos";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/CosaBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=5;
        		break;
    		case "Un papel":
        		id=19;
        		atk=4;
        		this.hp=hp;
        		name=nombre;
        		description="Es magico";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/PapelBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=6;
        		break;
    		case "Link":
        		id=20;
        		atk=6;
        		this.hp=hp;
        		name=nombre;
        		description="Un elfo :v";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/LinkBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=7;
        		break;
    		case "Un moco":
        		id=21;
        		atk=7;
        		this.hp=hp;
        		name=nombre;
        		description="Miralo";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/MocoBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=8;
        		break;
    		case "Un dragon":
        		id=22;
        		atk=3;
        		this.hp=hp;
        		name=nombre;
        		description="Un dragon guapo";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/DragonBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=9;
        		break;
    		case "Un palomazo":
        		id=23;
        		atk=10;
        		this.hp=hp;
        		name=nombre;
        		description="Te palomea";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/PalomazoBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=10;
        		break;
    		case "Basurilla":
        		id=24;
        		atk=3;
        		this.hp=hp;
        		name=nombre;
        		description="Del fondo del contenedor";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/BasuraBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=2;
        		break;
    		case "Dom S.":
        		id=25;
        		atk=4;
        		this.hp=hp;
        		name=nombre;
        		description="Compa�ero de Marcus F.";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/DomBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=4;
        		break;
    		case "Marcus F.":
        		id=26;
        		atk=5;
        		this.hp=hp;
        		name=nombre;
        		description="Muy mala ostia";
        		tipoCarta=TipoCarta.PROVOCAR;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/MarcusBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=6;
        		break;
    		case "Piper":
        		id=27;
        		atk=1;
        		this.hp=hp;
        		name=nombre;
        		description="Es asquerosa";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/PiperBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=1;
        		break;
    		case "Un perrete":
        		id=28;
        		atk=3;
        		this.hp=hp;
        		name=nombre;
        		description="A la yugular";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/PerreteBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=2;
        		break;
    		case "Un gatete":
        		id=29;
        		atk=4;
        		this.hp=hp;
        		name=nombre;
        		description="Te ara�a";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/GateteBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=3;
        		break;
    		case "Aragorn":
        		id=30;
        		atk=6;
        		this.hp=hp;
        		name=nombre;
        		description="Rey de Gondor";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/AragornBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=4;
        		break;
    		case "Yoda":
        		id=31;
        		atk=5;
        		this.hp=hp;
        		name=nombre;
        		description="Es verde";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/YodaBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=5;
        		break;
    		case "Un negro racista":
        		id=32;
        		atk=7;
        		this.hp=hp;
        		name=nombre;
        		description="Es como un shiny";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/NegroRacistaBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=6;
        		break;
    		case "Legolas":
        		id=33;
        		atk=8;
        		this.hp=hp;
        		name=nombre;
        		description="Que pelazo";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/LegolasBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=7;
        		break;
    		case "Una sandia":
        		id=34;
        		atk=8;
        		this.hp=hp;
        		name=nombre;
        		description="Esta pocha";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/SandiaBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=8;
        		break;
    		case "Bull":
        		id=35;
        		atk=10;
        		this.hp=hp;
        		name=nombre;
        		description="Te mete un cebollazo";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/BullBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=9;
        		break;
    		case "Ganondorf":
        		id=36;
        		atk=11;
        		this.hp=hp;
        		name=nombre;
        		description="Sonriente";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/GanondorfBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=10;
        		break;
    		case "Dumbo":
        		id=37;
        		atk=4;
        		this.hp=hp;
        		name=nombre;
        		description="Destruccion Masiva";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/DumboBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=6;
        		break;
    		case "Zed":
        		id=38;
        		atk=2;
        		this.hp=hp;
        		name=nombre;
        		description="Asombroso";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/ZedBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=1;
        		break;
    		case "Magikarp":
        		id=39;
        		atk=5;
        		this.hp=hp;
        		name=nombre;
        		description="Salpicadura";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/MagikarpBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=9;
        		break;
    		case "Una vena":
        		id=40;
        		atk=1;
        		this.hp=hp;
        		name=nombre;
        		description="Ahi esta la tia";
        		tipoCarta=TipoCarta.SIGILO;
        		try {
    				imagen=ImageIO.read(new File("src/imagenes1/VenasBueno.png"));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        		mana=0;
        		break;
        		
        		
    	}
    }
    
    public enum TipoCarta{
        NORMAL,
        PROVOCAR,
        SIGILO;
    }
    
    public void tipoCarta( TipoCarta tc){
        
        
    }

    public Carta(int mana, int atk, int hp, String name, String description, TipoCarta tipoCarta,BufferedImage imagen) {
    	
    	this.mana = mana;
        this.atk = atk;
        this.hp = hp;
        this.name = name;
        this.description = description;
        this.tipoCarta = tipoCarta;
        this.imagen=imagen;
    }
  public Carta (int id) {
	  this.id=id;
  }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TipoCarta getTipoCarta() {
        return tipoCarta;
    }

    public void setTipoCarta(TipoCarta tipoCarta) {
        this.tipoCarta = tipoCarta;
    }
    
    
    @Override
	public String toString() {
		return "Carta [id=" + id + ", mana=" + mana + ", atk=" + atk + ", hp=" + hp + ", name=" + name
				+ ", description=" + description + ", tipoCarta=" + tipoCarta + ", imagen=" + imagen + "]";
	}
/*
 * String infoCarta muestra la informacion de la carta
 */
	public String infoCarta(){
        String inf;
    	
    	inf="<html> Nombre: "+this.name+"<br>"
                + "Descripcion: "+this.description+"<br>"
                +"Atk: "+this.atk+"<br>"
                +"Hp: "+this.hp+"<br>"
                +"Mana: "+this.mana+"<br>"
                +"Tipo de Carta: "+this.tipoCarta+"<html>";
    	
    	return inf;
    }

	public BufferedImage getImagen() {
		return imagen;
	}

	public void setImagen(BufferedImage imagen) {
		this.imagen = imagen;
	}
    
    
    /* Funcion Atacar, Funcion cartas de mazo a mano
        Funcion cartas de mano a mesa, Funcion cartas
        de mesa a muerte, Funcion de cartas aleatorias
        en el mazo, Crear arrays con mazo, mano y tablero.
        Puedo crear 3 clases, tablero, mazo y mano, con dos variables
        dentro de cada clase, la mia y la del enemigo
    
    
    
    
    
    
    
    
    
    
    METODOS DE ARRAYS
    
    binarySearch	Busca un valor que le pasamos por parámetro, devuelve su posición. Debe estar ordenado.	Un array y un valor. Los dos del mismo tipo. Estos pueden ser un byte, char, double, float, int, long, short u objecto.	int
copyOf	Copia un array y lo devuelve en un nuevo array.	Un array y la longitud. Si se pasa del tamaño del array original, rellena los con ceros las posiciones sobrantes. Estos pueden ser un byte, char, double, float, int, long, short u objecto.	array del mismo tipo que se introduce
copyOfRange	Copia un array y lo devuelve en un nuevo array. Le indicamos la posición de origen y de destino.	Un array, posición origen y destino. Estos pueden ser un byte, char, double, float, int, long, short u objecto.	array del mismo tipo que se introduce.
equals	Indica si dos arrays son iguales.	Dos arrays del mismo tipo.	true o false
fill	Rellena un array con un valor que le indiquemos como parámetro.	Un array y el valor a rellenar. Estos pueden ser un byte, char, double, float, int, long, short u objecto.	No devuelve nada
sort	Ordena el array.	Un array.
Estos pueden ser un byte, char, double, float, int, long, short u objecto.	No devuelve nada
toString	Muestra el contenido del array pasado como parámetros	Un array. Estos pueden ser un byte, char, double, float, int, long, short u objecto.	Devuelve una cadena con el contenido del array.
    */
    
    
}

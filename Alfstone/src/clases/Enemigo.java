package clases;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import interfaces.TableroJuego;

public class Enemigo extends Usuario{

	private Ventanas ventana;
	
	
	private int y=200;
	private Usuario usuario;

	
	public Enemigo (String nombre, Ventanas v) {
		super(nombre);
		this.ventana=v;
		
		//
		/*
		 * Aqui se introduce el mazo de cada jefe. PREGUNTAR AL PROFE EN LA PANTALLA DE JEFES QUE TE SALGA LA IMAGEN Y PUEDAS GOLPEAR 
		 * AL JEFE. PODER ELEGIR ENTRE LOS JEFES
		 */
		switch(nombre) {
		case "JefeNormal":
			this.setNombre(nombre);
			this.setHp(30);
			try {
				this.setHeroe(ImageIO.read(new File("./src/imagenesJefes/JefeNormalBueno.png")));
				this.setMazo(new HashSet<Carta>());
				this.getMazo().add(new Carta ("Rotulillo",3));
				this.getMazo().add(new Carta ("Mosquito Cenec",3));
				this.getMazo().add(new Carta ("Alberto",4));
				this.getMazo().add(new Carta ("Silla torcida",7));
				this.getMazo().add(new Carta ("Teclado sucio",4));
				this.getMazo().add(new Carta ("Murloc enfadado",6));
				this.getMazo().add(new Carta ("Patricio",8));
				this.getMazo().add(new Carta ("Pingu",2));
				this.getMazo().add(new Carta ("Paloma N.1",10));
				this.getMazo().add(new Carta ("Gracian",15));
				this.getMazo().add(new Carta ("Un yonki",2));
				this.getMazo().add(new Carta ("Un pedrolo",12));
				this.getMazo().add(new Carta ("Antonio",5));
				this.getMazo().add(new Carta ("Una brocheta",3));
				this.getMazo().add(new Carta ("Walter",7));
				this.getMazo().add(new Carta ("La cosa",5));
				this.getMazo().add(new Carta ("Link",7));
				this.getMazo().add(new Carta ("Un dragon",20));
				this.getMazo().add(new Carta ("Basurilla",2));
				this.getMazo().add(new Carta ("Marcus F.",4));
				this.getMazo().add(new Carta ("Un perrete",2));
				this.getMazo().add(new Carta ("Aragorn",1));
				this.getMazo().add(new Carta ("Yoda",4));
				this.getMazo().add(new Carta ("Legolas",7));
				this.getMazo().add(new Carta ("Bull",1));
				this.getMazo().add(new Carta ("Dumbo",8));
				this.getMazo().add(new Carta ("Magikarp",12));
				this.getMazo().add(new Carta ("Zed",2));
				this.getMazo().add(new Carta ("Una vena",1));
				this.getMazo().add(new Carta ("Un gatete",2));
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
	
			break;
			
			
		case "JefeSigilo":
			this.setNombre(nombre);
			this.setMazo(new HashSet<Carta>());
			this.setHp(30);
			try {
				this.setHeroe(ImageIO.read(new File("./src/imagenesJefes/JefeSigiloBueno.png")));
				this.getMazo().add(new Carta("Piper",2));
				this.getMazo().add(new Carta("Un perrete",2));
				this.getMazo().add(new Carta("Un gatete",2));
				this.getMazo().add(new Carta("Aragorn",1));
				this.getMazo().add(new Carta("Yoda",4));
				this.getMazo().add(new Carta("Un negro racista",1));
				this.getMazo().add(new Carta("Legolas",7));
				this.getMazo().add(new Carta("Una sandia",9));
				this.getMazo().add(new Carta("Bull",1));
				this.getMazo().add(new Carta("Ganondorf",7));
				this.getMazo().add(new Carta("Dumbo",8));
				this.getMazo().add(new Carta("Zed",2));
				this.getMazo().add(new Carta ("Silla torcida",7));
				this.getMazo().add(new Carta ("Teclado sucio",4));
				this.getMazo().add(new Carta ("Murloc enfadado",6));
				this.getMazo().add(new Carta ("Patricio",8));
				this.getMazo().add(new Carta ("Pingu",2));
				this.getMazo().add(new Carta ("Paloma N.1",10));
				this.getMazo().add(new Carta ("Gracian",15));
				this.getMazo().add(new Carta ("Un yonki",2));
				this.getMazo().add(new Carta ("Un pedrolo",12));
				this.getMazo().add(new Carta ("Antonio",5));
				this.getMazo().add(new Carta ("Aragorn",1));
				this.getMazo().add(new Carta ("Yoda",4));
				this.getMazo().add(new Carta ("Legolas",7));
				this.getMazo().add(new Carta ("Bull",1));
				this.getMazo().add(new Carta ("Dumbo",8));
				this.getMazo().add(new Carta ("Magikarp",12));
				this.getMazo().add(new Carta ("Zed",2));
				this.getMazo().add(new Carta ("Una vena",1));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			break;
			
		case "JefeProvocar":
			this.setNombre(nombre);
			this.setMazo(new HashSet<Carta>());
			this.setHp(30);
			try {
				this.setHeroe(ImageIO.read(new File("./src/imagenesJefes/JefeProvocarBueno.png")));
				this.getMazo().add(new Carta("Piper",2));
				this.getMazo().add(new Carta("Un perrete",2));
				this.getMazo().add(new Carta("Un gatete",2));
				this.getMazo().add(new Carta("Aragorn",1));
		        this.getMazo().add(new Carta("Yoda",4));
		        this.getMazo().add(new Carta("Un negro racista",1));
		        this.getMazo().add(new Carta("Legolas",7));
		        this.getMazo().add(new Carta("Una sandia",9));
		        this.getMazo().add(new Carta("Bull",1));
		        this.getMazo().add(new Carta("Ganondorf",7));
		        this.getMazo().add(new Carta("Dumbo",8));
		        this.getMazo().add(new Carta("Zed",2));
		        this.getMazo().add(new Carta("Magikarp",12));
	            this.getMazo().add(new Carta("Una vena",1));  
	            this.getMazo().add(new Carta("Un perrete",2));
				this.getMazo().add(new Carta("Un gatete",2));
				this.getMazo().add(new Carta("Aragorn",1));
				this.getMazo().add(new Carta("Yoda",4));
				this.getMazo().add(new Carta("Un negro racista",1));
				this.getMazo().add(new Carta("Legolas",7));
				this.getMazo().add(new Carta("Una sandia",9));
				this.getMazo().add(new Carta("Bull",1));
				this.getMazo().add(new Carta ("Mosquito Cenec",3));
				this.getMazo().add(new Carta ("Alberto",4));
				this.getMazo().add(new Carta ("Silla torcida",7));
				this.getMazo().add(new Carta ("Teclado sucio",4));
				this.getMazo().add(new Carta ("Murloc enfadado",6));
				this.getMazo().add(new Carta ("Patricio",8));
				this.getMazo().add(new Carta ("Pingu",2));
				this.getMazo().add(new Carta ("Paloma N.1",10));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
	}
	//Esta funcion realiza el turno del enemigo
	public void turnoEnemigo() {
		
		pasarCartasAMano();
		mostrarCartasMano();
		//entra a seleccionar cartas si la dimension de tableroFuturo es diferente a 0
		if(this.getTableroFuturo().size()!=0) {
			seleccionarCartas();
		}
		//entra a seleccionar cartas Heroe si la dimension de tableroFuturo es diferente a 0
		if(this.getTableroFuturo().size()!=0) {
			seleccionarCartasHeroe();
		}
		//Al final de la funcion siempre va a pasar turno
		pasarTurno();
	}
	
	public void mostrarCartasMano() {
		JLabel carta;
		JPanel aux=null;
		//Esta funcion se encarga de mostrar las cartas de la mano. Como es una ia, las pasa directamente al tablero
		int y=190;
		ArrayList<Carta> cartaPasar=new ArrayList<Carta>(this.getMano().values());
		//Random para elegir cartas aleatorias
		Random r=new Random();
		do {
			Carta cartaAPasar=cartaPasar.get(r.nextInt(cartaPasar.size()));
			 //Panel para poner la carta
			aux=new JPanel();
			carta=new JLabel();
			carta.setIcon(new ImageIcon(cartaAPasar.getImagen().getScaledInstance(180, 180, BufferedImage.SCALE_SMOOTH)));
			aux.add(carta);
			aux.setBounds(y,230, carta.getIcon().getIconWidth(),carta.getIcon().getIconHeight());
			ventana.getTableroJuego().add(aux);
			aux.setOpaque(false);
			
			JPanel x=aux;
			//Pasa por parametros la carta a la funcion pasarCartaATablero
			pasarCartaATablero(cartaAPasar);
			x.setVisible(false);
			//La borra para no poder sacarla mas
			cartaPasar.remove(cartaAPasar);
		//Con esto hago una separacion entre las cartas
		y+=190;
		aux.setVisible(true);
		}while(cartaPasar.size()>0);
		
		}
	public void seleccionarCartasHeroe() {
		
		//Esta funcion se encarga de seleccionar una carta de nuestro heroe, y el panel del heroe enemigo para inflingirle da�o.
		Random r=new Random();
		Carta seleccionadaHeroe=this.getTableroFuturo().get(r.nextInt(this.getTableroFuturo().size()-1));
		//Pasa por parametro a la funcion atacarHeroes los valores
		atacarHeroes(seleccionadaHeroe,TableroJuego.miHeroe1);
	
	}
	
	//Con esta funcion se seleccionan las cartas que se pelearan entre ellas
		public void seleccionarCartas() {//poner Random
			
			Random r=new Random();
			
			
				Carta x=this.getTableroFuturo().get(r.nextInt(this.getTableroFuturo().size()-1));
				Carta y=ventana.getUsuarioLog().getTableroFuturo().get(r.nextInt(ventana.getUsuarioLog().getTableroFuturo().size()-1));
							//Se le a�aden por parametros las cartas a la funcion atacar Cartas, las cuales se atacan entre ellas
				atacarCartas(x,y);
			
			
	}
		//Con esta funcion se selecciona una carta, la cual ha sido cogida en mostrarCartas, y se pasa al tablero
	public void pasarCartaATablero(Carta actual) {
		JLabel jlabelTablero=new JLabel();
		
		this.setTablero(ventana.getTableroJuego().getTablero());
		//Se a�ade la carta al tablero
		this.getTablero().add(actual);
		//Se coloca en el JPanel
		JPanel [] cartasTablero=new JPanel[this.getTablero().size()];
		cartasTablero[this.getTablero().size()-1]=new JPanel();
		System.out.println (actual.toString());
		//jlabelTablero.setIcon(new ImageIcon(this.getMano().put(actual.getName(),actual).getImagen().getScaledInstance(180, 200, BufferedImage.SCALE_SMOOTH)));
		jlabelTablero.setIcon(new ImageIcon(actual.getImagen().getScaledInstance(180, 200, BufferedImage.SCALE_SMOOTH)));
		//Aqui se dice donde se colocan las cartas, y se a�ade a jlabel
		cartasTablero[this.getTablero().size()-1].setBounds(y+100, 300, jlabelTablero.getIcon().getIconWidth(), jlabelTablero.getIcon().getIconHeight());
        cartasTablero[this.getTablero().size()-1].add(jlabelTablero);
       
        ventana.getTableroJuego().add(cartasTablero[this.getTablero().size()-1], 0);
        //Se borra la carta de la mano para no poder seleccionarla mas
        this.getMano().remove(actual.getName(), actual);
		cartasTablero[this.getTablero().size()-1].setOpaque(false);
		
		
		y+=185;
	}
	//Con esta funcion se pasan un numero de cartas iniciales a la mano
	public void pasarCartasAMano() {
		ArrayList<Carta> arrayList = new ArrayList<Carta>(this.getMazo());
				
		Random cartas=new Random();
		int random=0;
		for(int i=0;i<7;i++) {
			System.out.println("arraylist size: "+arrayList.size());
			System.out.println("mazo size: "+this.getMazo().size());
			random=cartas.nextInt(arrayList.size());
			System.out.println("Random: "+random);
			//System.out.println(arrayList.get(random));
			Carta c=arrayList.get(random);
			this.getMano().put(c.getName(),c);
			arrayList.remove(random);
			this.getMazo().remove(c);
		}
		
	}

	//Con esta funcion se pasa el turno. La ia lo har� automatico
	public void pasarTurno() {
		this.getTableroFuturo().addAll(this.getTablero());
				
				ventana.getTableroJuego().turnoHeroe();
	}
	//Con esta funcion se cogen las dos cartas que han sido seleccionadas en seleccionarCartas() y se enfrentan entre ellas
	public void atacarCartas(Carta seleccionada,Carta seleccionadaEnemigo) {//NECESITA LOS PARAMETROS, PREGUNTAR
		seleccionada.getAtk();
		seleccionada.getHp();
		
		seleccionadaEnemigo.getAtk();
		seleccionadaEnemigo.getHp();
						
		int enemigoCartaHp=seleccionadaEnemigo.getHp() - seleccionada.getAtk();
		int heroeCartaHp=seleccionada.getHp() - seleccionadaEnemigo.getAtk();
						
		seleccionadaEnemigo.setHp(enemigoCartaHp);
		seleccionada.setHp(heroeCartaHp);
		//si la vida de la carta del enemigo es igual o menor a 0, se borra del tablero
						if(seleccionadaEnemigo.getHp()<=0) {
							ventana.getUsuarioLog().getTableroFuturo().remove(seleccionadaEnemigo);	
						}if(seleccionada.getHp()<=0) {
							//Si la vida de la carta de este jugador es igual o menor a 0, se borra del tablero
							this.getTableroFuturo().remove(seleccionada);
						}if(seleccionada.getHp()>0) {
							//Si no es igual o menor a 0, se devuelve a tablero, para que no pueda volver a atacar mas, ya que 
							//El arrayList que permite atacar es el arrayList de futuroTablero, y al pasarlo a tablero deja de atacar.
							//al darle a fin de turno, la carta vuelve a tableroFuturo
							this.getTablero().add(seleccionada);
							this.getTableroFuturo().remove(seleccionada);
							
						}
			
}
	//Coge las 2 cartas de la funcion seleccionarCartasHeroe() y lo enfrenta
public void atacarHeroes(Carta seleccionada, JPanel heroeEnemigo) {
			heroeEnemigo.getBounds();

		int heroeEnemigoHp=this.getHp()-seleccionada.getAtk();
		this.setHp(heroeEnemigoHp);
		if(this.getHp()<=0) {
			JOptionPane.showMessageDialog(ventana, "Has ganado!");
			ventana.cargarPantallaLobby();
		}else {
			this.getTableroFuturo().remove(seleccionada);
			this.getTablero().add(seleccionada); //ESTE ACABA LA FUNCION
}
}
}
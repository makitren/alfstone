/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfstone;

import static clases.Carta.TipoCarta.NORMAL;
import static clases.Carta.TipoCarta.PROVOCAR;
import static clases.Carta.TipoCarta.SIGILO;

import java.io.File;
import java.io.IOException;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JLabel;

import clases.Carta;

import clases.Ventanas;
import interfaces.CreacionMazo;
import interfaces.Lobby;
import interfaces.PantallaInicio;
import interfaces.PantallaLogin;
import interfaces.PantallaRegistro;
import interfaces.TableroJuego;

import javax.imageio.ImageIO;
import javax.swing.JComboBox;
/**
 *
 * @author 1DAM
 */
public class Alfstone {
	
    public static void main(String[] args)  {
      
    	/*
    	 * @param ArrayList de tipo carta para meter las cartas 
    	 */
         ArrayList<Carta>cartas=new ArrayList<Carta>(40);
        
         cartas.add(new Carta("Rotulillo",3));
         cartas.add(new Carta("Mosquito Cenec",3));
         cartas.add(new Carta("Alberto",4));
         cartas.add(new Carta("Silla torcida",7));
         cartas.add(new Carta("Teclado sucio",4));
         cartas.add(new Carta("Murloc enfadado",6));
         cartas.add(new Carta("Patricio",8));
         cartas.add(new Carta("Pingu",2));
         cartas.add(new Carta("Paloma N.1",10));
         cartas.add(new Carta("Gracian",15));
         cartas.add(new Carta("Un yonki",2));
         cartas.add(new Carta("Un pedrolo",12));
         cartas.add(new Carta("Antonio",5));
         cartas.add(new Carta("Cangrechillo",1));
         cartas.add(new Carta("Una brocheta",3));
         cartas.add(new Carta("Sheldon",4));
         cartas.add(new Carta("Walter",7));
         cartas.add(new Carta("La cosa",5));
         cartas.add(new Carta("Un papel",5));
         cartas.add(new Carta("Link",7));
         cartas.add(new Carta("Un moco",8));
         cartas.add(new Carta("Un dragon",20));  
         cartas.add(new Carta("Un palomazo",10));
         cartas.add(new Carta("Basurilla",2));
         cartas.add(new Carta("Dom S.",3));  
         cartas.add(new Carta("Marcus F.",4));    
         cartas.add(new Carta("Piper",2));
         cartas.add(new Carta("Un perrete",2));
         cartas.add(new Carta("Un gatete",2));  
         cartas.add(new Carta("Aragorn",1));
         cartas.add(new Carta("Yoda",4));
         cartas.add(new Carta("Un negro racista",1));
         cartas.add(new Carta("Legolas",7));
         cartas.add(new Carta("Una sandia",9));
         cartas.add(new Carta("Bull",1));
         cartas.add(new Carta("Ganondorf",7));
         cartas.add(new Carta("Dumbo",8));
         cartas.add(new Carta("Zed",2));
         cartas.add(new Carta("Magikarp",12));
         cartas.add(new Carta("Una vena",1));         
         
        /*
         * @param ventana variable tipo ventana inicia el programa
         * @return ventana
         */
         Ventanas v=new Ventanas(cartas);
         
    }
}

package interfaces;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextPane;

import baseDeDatos.BD;
import clases.Usuario;
import clases.Ventanas;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JTextField;
import java.awt.Color;

public class PantallaRegistro extends JPanel{
	private JTextField textNombreEscrito;
	private JTextField textAliasEscrito;
	private JPasswordField textContraseniaEscrito;
	private JTextField textEmailEscrito;
	private Connection bd;
	private Statement registerStatement;
	private Ventanas ventana;
	
	
	public PantallaRegistro(Ventanas v) {
		try {
			bd=DriverManager.getConnection(BD.bdNombre,BD.bdUsuario,BD.bdContraseña);
			registerStatement=bd.createStatement();
		}catch(SQLException e) {
			JOptionPane.showMessageDialog(ventana, "No se ha podido iniciar la conexion con la base de datos");
			e.printStackTrace();
		}
		
		this.ventana=v;
		this.setSize(1200,1000);
		setLayout(null);
		
		JTextPane txtpnCreacionDeGuerrero = new JTextPane();
		txtpnCreacionDeGuerrero.setForeground(Color.GRAY);
		txtpnCreacionDeGuerrero.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 45));
		txtpnCreacionDeGuerrero.setText("Creacion de Guerrero");
		txtpnCreacionDeGuerrero.setBounds(335, 13, 623, 85);
		txtpnCreacionDeGuerrero.setOpaque(false);
		add(txtpnCreacionDeGuerrero);
		
		JTextPane textNombre = new JTextPane();
		textNombre.setEditable(false);
		textNombre.setBackground(Color.LIGHT_GRAY);
		textNombre.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 24));
		textNombre.setText("Nombre");
		textNombre.setBounds(288, 225, 121, 40);
		add(textNombre);
		
		JTextPane textContraseña = new JTextPane();
		textContraseña.setBackground(Color.LIGHT_GRAY);
		textContraseña.setText("Contrase\u00F1a");
		textContraseña.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 24));
		textContraseña.setBounds(264, 383, 172, 40);
		add(textContraseña);
		
		JTextPane textEmail = new JTextPane();
		textEmail.setBounds(275, 455, 132, 34);
		add(textEmail);
		textEmail.setEditable(false);
		textEmail.setBackground(Color.LIGHT_GRAY);
		textEmail.setText("Email");
		textEmail.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 24));
		//Aqui se introduce el nombre del jugador
		textNombreEscrito = new JTextField();
		textNombreEscrito.setBackground(Color.BLACK);
		textNombreEscrito.setForeground(Color.RED);
		textNombreEscrito.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 22));
		textNombreEscrito.setBounds(747, 225, 197, 40);
		add(textNombreEscrito);
		textNombreEscrito.setColumns(10);
		//Aqui se introduce el alias del jugador
		textAliasEscrito = new JTextField();
		textAliasEscrito.setForeground(Color.RED);
		textAliasEscrito.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 22));
		textAliasEscrito.setBackground(Color.BLACK);
		textAliasEscrito.setBounds(747, 304, 197, 40);
		add(textAliasEscrito);
		textAliasEscrito.setColumns(10);
		
		JTextPane TextAlias = new JTextPane();
		TextAlias.setBounds(275, 302, 145, 40);
		add(TextAlias);
		TextAlias.setEditable(false);
		TextAlias.setBackground(Color.LIGHT_GRAY);
		TextAlias.setText("Alias");
		TextAlias.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 24));
		//Aqui se introduce la contraseña del jugador
		textContraseniaEscrito = new JPasswordField();
		textContraseniaEscrito.setForeground(Color.RED);
		textContraseniaEscrito.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 21));
		textContraseniaEscrito.setBackground(Color.BLACK);
		textContraseniaEscrito.setBounds(747, 383, 197, 40);
		add(textContraseniaEscrito);
		textContraseniaEscrito.setColumns(10);
		//Aqui se introduce el email del jugador
		textEmailEscrito =new JTextField();
		textEmailEscrito.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 21));
		textEmailEscrito.setForeground(Color.RED);
		textEmailEscrito.setBackground(Color.BLACK);
		textEmailEscrito.setBounds(747, 468, 197, 40);
		add(textEmailEscrito);
		textEmailEscrito.setColumns(10);
		
		String.valueOf(textContraseniaEscrito.getPassword());
		
		
		
		
		
		 BufferedImage fondoPantalla=null;
		  setLayout(null);
		JPanel panel=new JPanel();
		try {
			fondoPantalla=ImageIO.read(new File("./src/imagenesInterfaces/FondoCrMa.png"));
		}catch (IOException e) {
			e.printStackTrace();
			System.err.println("No se puede leer la imagen");
		}
		
		JButton botonAtras = new JButton("Volver");
		botonAtras.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 18));
		botonAtras.setBounds(30, 774, 172, 51);
		add(botonAtras);
		botonAtras.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ventana.cargarPantallaInicio();
			}
		});
		JButton botonRegistro = new JButton("Registrarse");
		botonRegistro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				registrarUsuario();
				
				
			}
		});
		botonRegistro.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 18));
		botonRegistro.setBounds(921, 774, 179, 51);
		add(botonRegistro);
		
		JLabel fondo=new JLabel(new ImageIcon(fondoPantalla));
		panel.setBounds(0,0,this.getWidth(),this.getHeight());
		panel.setLayout(null);
		panel.add(fondo);
		fondo.setBounds(0,0,this.getWidth(),this.getHeight());
		  add(panel);
		this.setVisible(true);
	
}
	
	public void registrarUsuario() {
		try {
			Usuario usuarioRegistrado= new Usuario(textNombreEscrito.getText(),textAliasEscrito.getText(),String.valueOf(textContraseniaEscrito.getPassword()),textEmailEscrito.getText());
			registerStatement.execute("INSERT INTO Usuario (nombre,alias,contrasenia,email) VALUES ('"+usuarioRegistrado.getNombre()+
					"','"+usuarioRegistrado.getAlias()+
					"','"+usuarioRegistrado.getContraseña()+
					"','"+usuarioRegistrado.getEmail()+"')");
				registerStatement.close();
				JOptionPane.showMessageDialog(this,"El usuario ha sido introducido correctamente ");
				ventana.cargarPantallaLobby();
			
			
		}catch(SQLException e) {
			JOptionPane.showMessageDialog(this, "Error al introducir los datos del usuario");
			e.printStackTrace();
		}
	}
}

package interfaces;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import clases.Ventanas;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PantallaInicio extends JPanel{
	private JTextField txtSaludosInvocadorcon;
	private JTextField txtNuevo;
	private JTextField txtIniciar;
	private JTextField txtAsustado;
	private Ventanas ventana;
	
	
	public PantallaInicio(Ventanas v) {
	this.ventana=v;
		this.setSize(1200,1000);
		 setLayout(null);
		 
		
		txtSaludosInvocadorcon = new JTextField();
		txtSaludosInvocadorcon.setEditable(false);
		txtSaludosInvocadorcon.setBackground(Color.BLACK);
		
		txtSaludosInvocadorcon.setForeground(Color.RED);
		txtSaludosInvocadorcon.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 30));
		txtSaludosInvocadorcon.setText("Saludos, Invocador. \u00BFCon qu\u00E9 acci\u00F3n nos honrar\u00E1s hoy?");
		txtSaludosInvocadorcon.setBounds(71, 26, 970, 40);
		 add(txtSaludosInvocadorcon);
		txtSaludosInvocadorcon.setColumns(10);
		
		txtNuevo = new JTextField();
		txtNuevo.setEditable(false);
		txtNuevo.setForeground(Color.RED);
		txtNuevo.setBackground(Color.BLACK);
		txtNuevo.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 25));
		txtNuevo.setText("\u00BFNuevo en el juego? Entra aqu\u00ED");
		txtNuevo.setBounds(95, 235, 443, 89);
		 add(txtNuevo);
		txtNuevo.setColumns(10);
		
		txtIniciar = new JTextField();
		txtIniciar.setEditable(false);
		txtIniciar.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 25));
		txtIniciar.setBackground(Color.BLACK);
		txtIniciar.setForeground(Color.RED);
		txtIniciar.setText("\u00BFYa tienes cuenta? Inicia sesi\u00F3n");
		txtIniciar.setBounds(95, 412, 433, 89);
		 add(txtIniciar);
		txtIniciar.setColumns(10);
		
		txtAsustado = new JTextField();
		txtAsustado.setEditable(false);
		txtAsustado.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 25));
		txtAsustado.setForeground(Color.RED);
		txtAsustado.setBackground(Color.BLACK);
		txtAsustado.setText("\u00BFAsustado? \u00A1Largo de aqu\u00ED!");
		txtAsustado.setBounds(95, 592, 433, 76);
		 add(txtAsustado);
		txtAsustado.setColumns(10);
		
		//Boton que lleva a la pantalla de registro de Usuario
		JButton btnInicio = new JButton("Registrarse");
		btnInicio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventana.cargarPantallaRegistroUsuario();
			}
		});
		btnInicio.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 19));
		btnInicio.setForeground(new Color(255, 255, 0));
		btnInicio.setBackground(new Color(139, 0, 0));
		btnInicio.setBounds(718, 236, 190, 76);
		 add(btnInicio);
		
		 //Boton que lleva a la pantalla de inicio de sesion
		JButton btnLogin = new JButton("Iniciar Sesion");
		btnLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ventana.cargarPantallaInicioSesion();
			}
		});
		btnLogin.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 19));
		btnLogin.setForeground(new Color(255, 255, 0));
		btnLogin.setBackground(new Color(139, 0, 0));
		btnLogin.setBounds(724, 411, 184, 76);
		 add(btnLogin);
		//Boton que apaga la aplicacion
		JButton btnAdios = new JButton("Adi\u00F3s");
		btnAdios.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ventana.dispose();
			}
		});
		btnAdios.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 19));
		btnAdios.setBackground(new Color(139, 0, 0));
		btnAdios.setForeground(Color.YELLOW);
		btnAdios.setBounds(724, 592, 179, 76);
		 add(btnAdios);
		//Fondo de pantalla
		 JLabel lblFondo = new JLabel();
		lblFondo.setBounds(12, 0, 1200, 1000);
		
		BufferedImage fondoPantalla=null;
		   setLayout(null);
		JPanel panel=new JPanel();
		try {
			fondoPantalla=ImageIO.read(new File("./src/imagenesInterfaces/FondoPantallaInicio.jpg"));
		}catch (IOException e) {
			e.printStackTrace();
			System.err.println("No se puede leer la imagen");
		}
		
		JLabel fondo=new JLabel(new ImageIcon(fondoPantalla));
		panel.setBounds(0,0,this.getWidth(),this.getHeight());
		panel.setLayout(null);
		panel.add(fondo);
		fondo.setBounds(0,0,this.getWidth(),this.getHeight());
		   add(panel);
		this.setVisible(true);
		
		
		
		
	}
}

package interfaces;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import clases.Carta;
import clases.Enemigo;
import clases.Usuario;
import clases.Ventanas;
import javax.swing.JButton;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class TableroJuego extends JPanel {
	private Enemigo enemigo;
	private Ventanas ventana;
	private JLabel jlabel;
	private JPanel heroeEnemigo1;
	private Carta cartaUsuarioElegida;
	private Carta cartaUsuarioElegidaEnemigo;
	public static JPanel miHeroe1;
	public ArrayList<Carta> getTablero() {
		return tablero;
	}
	public void setTablero(ArrayList<Carta> tablero) {
		this.tablero = tablero;
	}

	private int y=200;
	private ArrayList<Carta> tableroFuturo= new ArrayList<Carta>();
	private ArrayList<Carta> tablero = new ArrayList<Carta>(7);
	JPanel [] cartasTablero;
	
	public TableroJuego(Ventanas v,Enemigo e) {
		enemigo=e;
		ventana=v;
		//ventana.getEnemigo().getMazo();
		setLayout(null);
		this.setSize(1700,1020);
	
				turnoHeroe();	
			
		/*	enemigo.pasarCartasAManoIniciales();
			enemigo.turnoEnemigo();
			*/
			
			
		
		// rodear el programa con un do while para que se repita hasta que la vida del jefe llegue a 0
			
			
			
			try {
				ventana.getUsuarioLog().setHeroe(ImageIO.read(new File("./src/imagenesJefes/JefeNormalBueno.png")));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			JLabel miHeroe=new JLabel();
			JPanel miHeroe1=new JPanel();
			miHeroe1.setBounds(750,620,170,240);
			miHeroe1.add(miHeroe);
			miHeroe.setIcon(new ImageIcon(ventana.getUsuarioLog().getHeroe().getScaledInstance(170, 240, BufferedImage.SCALE_SMOOTH)));
			add(miHeroe1,0);
			miHeroe1.setVisible(true);
			miHeroe1.setOpaque(false);
			
			JLabel heroeEnemigo=new JLabel();
			 heroeEnemigo1=new JPanel();
			heroeEnemigo1.setBounds(750,120,170,240);
			heroeEnemigo1.add(heroeEnemigo);
			heroeEnemigo.setIcon(new ImageIcon(ventana.getEnemigo().getHeroe().getScaledInstance(170, 240, BufferedImage.SCALE_SMOOTH)));
			add(heroeEnemigo1,0);
			heroeEnemigo1.setVisible(true);
			heroeEnemigo1.setOpaque(false);
			
			
		
		BufferedImage fondoPantalla=null;
		setLayout(null);
		JPanel panel=new JPanel();
		
		try {
			fondoPantalla=ImageIO.read(new File("./src/imagenesInterfaces/TableroBueno.png"));
		}catch (IOException eX) {
			eX.printStackTrace();
			System.err.println("No se puede leer la imagen");
		}
		
			
		JLabel fondo=new JLabel(new ImageIcon(fondoPantalla));
		panel.setBounds(0,13,this.getWidth(),this.getHeight());
		panel.add(fondo);
		fondo.setBounds(0,0,this.getWidth(),this.getHeight());
		add(panel);
		this.setVisible(true);
		
		
		
		
	}
	//Esta funcion se encarga de ejecutar el turno del heroe
	public void turnoHeroe() {
		pasarCartasAMano();
		mostrarCartasMano();
		if(tableroFuturo.size()!=0) {
			seleccionarCartasPelea();
		}
		if(tableroFuturo.size()!=0) {
			seleccionarCartasHeroe();
		}
		pasarTurno();
	}
	public void seleccionarCartasHeroe() {
		Iterator it=tableroFuturo.iterator();
		while(it.hasNext()) {
		Carta seleccionadaHeroe=(Carta)it.next();
		seleccionadaHeroe.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked (MouseEvent arg0) {
		
		atacarHeroes(seleccionadaHeroe,heroeEnemigo1);
		}
		});
		}

	}
	
	//Esta funcion se encarga de seleccionar una carta del tablero para mas tarde poder usarla para atacar
	public void seleccionarCartasPelea() {//PREGUNTAR POR LA FUNCION DE ATACAR, QUE HAY QUE PASAR POR PARAMETROS PARA QUE PUEDA FUNCIONAR
							   //Y VER SI LA FUNCION EST� BIEN. EL PROBLEMA ES QUE NO SE QUE HAY QUE PASAR POR PARAMETROS A LA 
								// FUNCION DE ATACAR PARA QUE FUNCIONE
		Iterator it=tableroFuturo.iterator();
		System.out.println(enemigo.getTableroFuturo());
		Iterator itEnemigo=enemigo.getTableroFuturo().iterator();
		while(it.hasNext()) {
			Carta seleccionadaHeroe=(Carta)it.next();
			seleccionadaHeroe.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					cartaUsuarioElegida=seleccionadaHeroe;
				}
				});
		}
		while(itEnemigo.hasNext()) {
			Carta seleccionadaEnemigo=(Carta)itEnemigo.next();
			seleccionadaEnemigo.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					cartaUsuarioElegidaEnemigo=seleccionadaEnemigo;
					if(cartaUsuarioElegida!=null) {
						atacarCartas(cartaUsuarioElegida,cartaUsuarioElegidaEnemigo);
						cartaUsuarioElegida=null;
						cartaUsuarioElegidaEnemigo=null;
					}
				}
				});
		}
	
		
	}

	//Esta funcion muestra las cartas de la mano
	public void mostrarCartasMano() {
		JLabel carta;
		JPanel aux=null;
		
		int y=190;
		Iterator it=ventana.getUsuarioLog().getMano().values().iterator();
			while(it.hasNext()) {
				Carta actual=(Carta)it.next();
				aux=new JPanel();
				carta=new JLabel();
				carta.setIcon(new ImageIcon(actual.getImagen().getScaledInstance(180, 180, BufferedImage.SCALE_SMOOTH)));
				aux.add(carta);
				aux.setBounds(y,830, carta.getIcon().getIconWidth(),carta.getIcon().getIconHeight());
				add(aux);
				aux.setOpaque(false);
				actual.toString();
				JPanel x=aux;
				aux.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						if(tablero.size()<7) {
							pasarCartaATablero(actual);
							x.setVisible(false);
						}else {
							JOptionPane.showMessageDialog(ventana, "No puedes tener mas de 7 cartas en el tablero");
						}
						
						
					}
				});
			
			y+=190;
			aux.setVisible(true);
		}
			
	}
	
	//Esta funcion se encarga de pasar las cartas que clickee al tablero
	public void pasarCartaATablero(Carta actual) {
		JLabel jlabelTablero=new JLabel();
		
		ventana.getUsuarioLog().setTablero(tablero);
		ventana.getUsuarioLog().getTablero().add(actual);
		 cartasTablero=new JPanel[ventana.getUsuarioLog().getTablero().size()];
		cartasTablero[ventana.getUsuarioLog().getTablero().size()-1]=new JPanel();
		System.out.println (actual.toString());
		//jlabelTablero.setIcon(new ImageIcon(ventana.getUsuarioLog().getMano().put(actual.getName(),actual).getImagen().getScaledInstance(180, 200, BufferedImage.SCALE_SMOOTH)));
		jlabelTablero.setIcon(new ImageIcon(actual.getImagen().getScaledInstance(180, 200, BufferedImage.SCALE_SMOOTH)));

		cartasTablero[ventana.getUsuarioLog().getTablero().size()-1].setBounds(y+100, 525, jlabelTablero.getIcon().getIconWidth(), jlabelTablero.getIcon().getIconHeight());
        cartasTablero[ventana.getUsuarioLog().getTablero().size()-1].add(jlabelTablero);
        
        add(cartasTablero[ventana.getUsuarioLog().getTablero().size()-1], 2);
        ventana.getUsuarioLog().getMano().remove(actual.getName(), actual);
		cartasTablero[ventana.getUsuarioLog().getTablero().size()-1].setOpaque(false);
		
		
		y+=185;
	}
//Esta funcion se encarga de pasar las cartas del mazo a la mano
	public void pasarCartasAMano() {
		ArrayList<Carta> arrayList = new ArrayList<Carta>(ventana.getUsuarioLog().getMazo());
		System.out.println(arrayList.size());
		Random cartas=new Random();
		int random=0;
		for(int i=0;i<9;i++) {
			System.out.println("arraylist size: "+arrayList.size());
			System.out.println("mazo size: "+ventana.getUsuarioLog().getMazo().size());
			random=cartas.nextInt(arrayList.size());
			System.out.println("Random: "+random);
			ventana.getUsuarioLog().getMano().put(arrayList.get(random).getName(),arrayList.get(random));
			System.out.println(ventana.getUsuarioLog().getMano().toString());
			arrayList.remove(random);
		}
		
	}
//Esta funcion permite pasar el turno y que comience el del enemigo
	
	public void pasarTurno() {
		JLabel pasarTurno=new JLabel();
		JPanel botonPasarTurno=new JPanel();
		botonPasarTurno.setBounds(1300,460,125,60);
		botonPasarTurno.add(pasarTurno);
		add(botonPasarTurno,1);
		botonPasarTurno.setVisible(true);
		botonPasarTurno.setOpaque(false);
		botonPasarTurno.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				tableroFuturo.addAll(ventana.getUsuarioLog().getTablero());
				enemigo.turnoEnemigo();
				
				
			}
		});
	}
	
public void atacarCartas(Carta seleccionada,Carta seleccionadaEnemigo) {//NECESITA LOS PARAMETROS, PREGUNTAR
		
			seleccionada.getAtk();
			seleccionada.getHp();
			
			seleccionadaEnemigo.getAtk();
			seleccionadaEnemigo.getHp();
							
			int enemigoCartaHp=seleccionadaEnemigo.getHp() - seleccionada.getAtk();
			int heroeCartaHp=seleccionada.getHp() - seleccionadaEnemigo.getAtk();
							
			seleccionadaEnemigo.setHp(enemigoCartaHp);
			seleccionada.setHp(heroeCartaHp);
							if(seleccionadaEnemigo.getHp()<=0) {
								enemigo.getTableroFuturo().remove(seleccionadaEnemigo);	
							}if(seleccionada.getHp()<=0) {
								tableroFuturo.remove(seleccionada);
							}if(seleccionada.getHp()>0) {
								tableroFuturo.remove(seleccionada);
								tablero.add(seleccionada);
							}
		//ESTE ACABA LA FUNCION
		}
	
		public void atacarHeroes(Carta seleccionada, JPanel heroeEnemigo) {
	 			heroeEnemigo.getBounds();
	
				int heroeEnemigoHp=ventana.getEnemigo().getHp()-seleccionada.getAtk();
				ventana.getEnemigo().setHp(heroeEnemigoHp);
				if(ventana.getEnemigo().getHp()<=0) {
					JOptionPane.showMessageDialog(ventana, "Has ganado!");
					ventana.cargarPantallaLobby();
				}else {
					tableroFuturo.remove(seleccionada);
					tablero.add(seleccionada); 
					
		
}
}
}

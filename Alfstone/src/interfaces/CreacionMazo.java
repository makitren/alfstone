package interfaces;

import javax.swing.JPanel;

import clases.Carta;
import clases.Carta.TipoCarta;

import clases.Ventanas;
import excepciones.InsertarImagenFallidoException;
import excepciones.MazoMuyLargoException;
import javafx.scene.layout.Background;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import static clases.Carta.TipoCarta.NORMAL;
import static clases.Carta.TipoCarta.PROVOCAR;
import static clases.Carta.TipoCarta.SIGILO;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CreacionMazo extends JPanel {
	private ArrayList<Carta>carta;
	private Ventanas ventana;
	private Statement registerStatement;
	private Connection BD;
	
	
	
	
	public CreacionMazo(Ventanas v,ArrayList<Carta>carta) {
		
		this.ventana=v;
		this.carta=carta;
		
		//Conexion con la base de datos
		try {
			BD=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/Alfstone?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","");
			registerStatement=BD.createStatement();
		}catch(SQLException e) {
			JOptionPane.showInputDialog("Conexion a BD fallida");
			e.printStackTrace();
		}
		
		
		//setSize de la Pantalla Principal en la que se carga la creacion del Mazo
		 setForeground(Color.LIGHT_GRAY);
		this.setSize(1200,1000);
		
		
		JPanel panelImagen = new JPanel();
		panelImagen.setOpaque(false);
		panelImagen.setBounds(57, 281, 400, 543);
		  add(panelImagen);
		 
		 
		 JPanel panelDatos = new JPanel();
		 panelDatos.setBackground(Color.BLACK);
			panelDatos.setBounds(599, 339, 306, 340);
			
			  add(panelDatos);
			 panelDatos.setLayout(null);	 
			 JLabel lblpanelDatos = new JLabel();
			 lblpanelDatos.setFont(new Font("Tahoma", Font.PLAIN, 32));
			 
			 lblpanelDatos.setForeground(Color.RED);
			
			 lblpanelDatos.setBounds(0, 0, 306, 340);
			 
			 panelDatos.add(lblpanelDatos);
			 JLabel labelImagen = new JLabel();
				panelImagen.add(labelImagen);
		
		
		/*
		 * ComboBox para elegir la carta mediante un SelectedIndex
		 */
		JComboBox cartas = new JComboBox();
		cartas.setBackground(Color.GRAY);
		cartas.setFont(new Font("Algerian", Font.PLAIN, 30));
		//carta.getName Para sacar el nombre de la carta por comboBox
		cartas.setModel(new DefaultComboBoxModel(new String[] {carta.get(0).getName(), carta.get(1).getName(), carta.get(2).getName(),
				carta.get(3).getName(), carta.get(4).getName(), carta.get(5).getName(), carta.get(6).getName(), carta.get(7).getName(), 
				carta.get(8).getName(), carta.get(9).getName(), carta.get(10).getName(), carta.get(11).getName(), carta.get(12).getName(),
				carta.get(13).getName(), carta.get(14).getName(), carta.get(15).getName(), carta.get(16).getName(), carta.get(17).getName(),
				carta.get(18).getName(), carta.get(19).getName(), carta.get(20).getName(), carta.get(21).getName(), carta.get(22).getName(),
				carta.get(23).getName(), carta.get(24).getName(), carta.get(25).getName(), carta.get(26).getName(), carta.get(27).getName(),
				carta.get(28).getName(), carta.get(29).getName(), carta.get(30).getName(), carta.get(31).getName(), carta.get(32).getName(),
				carta.get(33).getName(), carta.get(34).getName(), carta.get(35).getName(), carta.get(36).getName(), carta.get(37).getName(),
				carta.get(38).getName(), carta.get(39).getName()}));
		cartas.setBounds(137, 151, 282, 76);
		  add(cartas);
		  
		  /*
		   * actionPerformed para coger la carta del comboBox
		   */
		cartas.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				
				Carta x=carta.get(cartas.getSelectedIndex());//ALTERNATIVA A SELECTED INDEX DE ARRAY.
				labelImagen.setIcon(new ImageIcon(x.getImagen()));//SOLUCIONAR ERROR CON SELECTEDINDEX
				lblpanelDatos.setText(x.infoCarta());
				panelDatos.add(lblpanelDatos);
				
			}
		});
		//Boton seleccionar para meter la carta en el mazo
		JButton Seleccionar = new JButton("Seleccionar");
		Seleccionar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					escogerCarta(cartas);
				} catch (MazoMuyLargoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		Seleccionar.setFont(new Font("Algerian", Font.PLAIN, 19));
		Seleccionar.setBounds(660, 115, 190, 57);
		  add(Seleccionar);
		  
		  
		//Boton deseleccionar para quitar la carta del mazo
		JButton Deseleccionar = new JButton("Deseleccionar");
		Deseleccionar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				deseleccionarCarta(cartas);
			}
		});
		Deseleccionar.setFont(new Font("Algerian", Font.PLAIN, 19));
		Deseleccionar.setBounds(660, 201, 190, 57);
		  add(Deseleccionar);
		
		  //Boton volver para salir de la pantalla de Creacion de Mazo
		JButton botonAtras = new JButton("Volver");
		botonAtras.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ventana.cargarPantallaLobby();
			}
		});
		botonAtras.setBounds(117, 852, 149, 55);
		add(botonAtras);
		botonAtras.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 22));
		
		//Fondo de pantalla
		JLabel lblCreacionDeMazo = new JLabel("CREACION DE MAZO");
		lblCreacionDeMazo.setForeground(new Color(30, 144, 255));
		lblCreacionDeMazo.setFont(new Font("Algerian", Font.PLAIN, 40));
		lblCreacionDeMazo.setHorizontalAlignment(SwingConstants.CENTER);
		
		lblCreacionDeMazo.setBounds(150, 24, 778, 92);
		  add(lblCreacionDeMazo);
		
		 BufferedImage fondoPantalla=null;
		  setLayout(null);
		JPanel panel=new JPanel();
		try {
			fondoPantalla=ImageIO.read(new File("./src/imagenesInterfaces/FondoCrMa.png"));
		}catch (IOException e) {
			e.printStackTrace();
			System.err.println("No se puede leer la imagen");
		}
		JLabel fondo=new JLabel(new ImageIcon(fondoPantalla));
		
		panel.setBounds(0,0,this.getWidth(),this.getHeight());
		panel.setLayout(null);
		
		//Boton Mazo Automatico para no tener que introducir todas las cartas
		JButton btnMazoAutomatico = new JButton("Mazo automatico");
		btnMazoAutomatico.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventana.getUsuarioLog().getMazo().add(new Carta("Rotulillo",3));
				ventana.getUsuarioLog().getMazo().add(new Carta("Mosquito Cenec",3));
				ventana.getUsuarioLog().getMazo().add(new Carta("Alberto",4));
				ventana.getUsuarioLog().getMazo().add(new Carta("Silla torcida",7));
				ventana.getUsuarioLog().getMazo().add(new Carta("Teclado sucio",4));
				ventana.getUsuarioLog().getMazo().add(new Carta("Murloc enfadado",6));
				ventana.getUsuarioLog().getMazo().add(new Carta("Patricio",8));
				ventana.getUsuarioLog().getMazo().add(new Carta("Pingu",2));
				ventana.getUsuarioLog().getMazo().add(new Carta("Paloma N.1",10));
				ventana.getUsuarioLog().getMazo().add(new Carta("Gracian",15));
				ventana.getUsuarioLog().getMazo().add(new Carta("Un yonki",2));
				ventana.getUsuarioLog().getMazo().add(new Carta("Un pedrolo",12));
				ventana.getUsuarioLog().getMazo().add(new Carta("Antonio",5));
				ventana.getUsuarioLog().getMazo().add(new Carta("Cangrechillo",1));
				ventana.getUsuarioLog().getMazo().add(new Carta("Una brocheta",3));
				ventana.getUsuarioLog().getMazo().add(new Carta("Sheldon",4));
				ventana.getUsuarioLog().getMazo().add(new Carta("Walter",7));
				ventana.getUsuarioLog().getMazo().add(new Carta("La cosa",5));
				ventana.getUsuarioLog().getMazo().add(new Carta("Un papel",5));
				ventana.getUsuarioLog().getMazo().add(new Carta("Link",7));
				ventana.getUsuarioLog().getMazo().add(new Carta("Un moco",8));
				ventana.getUsuarioLog().getMazo().add(new Carta("Un dragon",20));  
				ventana.getUsuarioLog().getMazo().add(new Carta("Un palomazo",10));
				ventana.getUsuarioLog().getMazo().add(new Carta("Basurilla",2));
				ventana.getUsuarioLog().getMazo().add(new Carta("Dom S.",3));  
				ventana.getUsuarioLog().getMazo().add(new Carta("Marcus F.",4));    
				ventana.getUsuarioLog().getMazo().add(new Carta("Piper",2));
				ventana.getUsuarioLog().getMazo().add(new Carta("Un perrete",2));
				ventana.getUsuarioLog().getMazo().add(new Carta("Un gatete",2));
				ventana.getUsuarioLog().getMazo().add(new Carta("Aragorn",1));
	         System.out.println(ventana.getUsuarioLog().getMazo().size());
			}
		});
		btnMazoAutomatico.setBounds(477, 192, 129, 69);
		panel.add(btnMazoAutomatico);
		panel.add(fondo);
		fondo.setBounds(0,0,this.getWidth(),this.getHeight());
		  add(panel);
		this.setVisible(true);
	}
	/*
	 * Funcion para mostrar las imagenes de las cartas al hacer el selectedIndex en el comboBox
	 */
	public ArrayList<Carta> mostrarImagen (){
		ArrayList<Carta>carta=new ArrayList<Carta>();
		
			carta.add(new Carta("Rotulillo",3));
	         carta.add(new Carta("Mosquito Cenec",3));
	         carta.add(new Carta("Alberto",4));
	         carta.add(new Carta("Silla torcida",7));
	         carta.add(new Carta("Teclado sucio",4));
	         carta.add(new Carta("Murloc enfadado",6));
	         carta.add(new Carta("Patricio",8));
	         carta.add(new Carta("Pingu",2));
	         carta.add(new Carta("Paloma N.1",10));
	         carta.add(new Carta("Gracian",15));
	         carta.add(new Carta("Un yonki",2));
	         carta.add(new Carta("Un pedrolo",12));
	         carta.add(new Carta("Antonio",5));
	         carta.add(new Carta("Cangrechillo",1));
	         carta.add(new Carta("Una brocheta",3));
	         carta.add(new Carta("Sheldon",4));
	         carta.add(new Carta("Walter",7));
	         carta.add(new Carta("La cosa",5));
	         carta.add(new Carta("Un papel",5));
	         carta.add(new Carta("Link",7));
	         carta.add(new Carta("Un moco",8));
	         carta.add(new Carta("Un dragon",20));  
	         carta.add(new Carta("Un palomazo",10));
	         carta.add(new Carta("Basurilla",2));
	         carta.add(new Carta("Dom S.",3));  
	         carta.add(new Carta("Marcus F.",4));    
	         carta.add(new Carta("Piper",2));
	         carta.add(new Carta("Un perrete",2));
	         carta.add(new Carta("Un gatete",2));
	         carta.add(new Carta("Aragorn",1));
	         carta.add(new Carta("Yoda",4));
	         carta.add(new Carta("Un negro racista",1));
	         carta.add(new Carta("Legolas",7));
	         carta.add(new Carta("Una sandia",9));
	         carta.add(new Carta("Bull",1));
	         carta.add(new Carta("Ganondorf",7));
	         carta.add(new Carta("Dumbo",8));
	         carta.add(new Carta("Zed",2));
	         carta.add(new Carta("Magikarp",12));
	         carta.add(new Carta("Una vena",1));
		
		return carta;
	}
	/*
	 * Funcion para escoger la Carta del comboBox
	 */
	public void  escogerCarta(JComboBox cartas) throws MazoMuyLargoException{
		
		
		//Investigar el size de getMazoJugador, y preguntar por que al tener una carta deseleccionada sigue diciendo al pulsar el boton que se ha deseleccionado, y no pasa por el error.
		try {
			if(ventana.getUsuarioLog().getMazo().size()<30) {
			/*
			 * @param registerStatement para ejecutar los inserts y aņadirlos a la base de datos
			 */
				Carta x=carta.get(cartas.getSelectedIndex());
				registerStatement.execute("INSERT INTO Usuario_has_Cartas (usuario,idCartas) VALUES ('"+ventana.getUsuarioLog().getEmail()+"' ,'"+x.getId()+"')");	
				ventana.getUsuarioLog().aņadirAlMazo(x);
				System.out.println(ventana.getUsuarioLog().getMazo().size());	
			}	
			/*
			 * @param MazoMuyLargoException excepcion propia para cuando hay mas de 30 cartas
			 */
		}catch(MazoMuyLargoException ex) {
			JOptionPane.showMessageDialog(ventana,"No puedes tener mas de 30 cartas en el mazo");
			ex.printStackTrace();
		}catch(SQLException e) {
			JOptionPane.showMessageDialog(ventana,"Error al introducir la carta, puede ser por que la carta este duplicada");
			e.printStackTrace();
		}
		
	}
	public void deseleccionarCarta(JComboBox cartas) {
		
		try {
			Carta x=carta.get(cartas.getSelectedIndex());
			/*
			 * @param registerStatement para borrar la carta de la base de datos
			 */
			registerStatement.execute("DELETE FROM Usuario_has_Cartas WHERE idCartas = "+x.getId()+" AND usuario= '"+ventana.getUsuarioLog().getEmail()+"'");
			ventana.getUsuarioLog().getMazo().remove(x);
			System.out.println(ventana.getUsuarioLog().getMazo().size());
			JOptionPane.showMessageDialog(ventana,"La carta "+x.getName()+" ya no forma parte de tu mazo");
		}catch(SQLException e) {
			JOptionPane.showMessageDialog(ventana,"Error al deseleccionar la carta del mazo, es posible que no estuviera en tu mazo");
			e.printStackTrace();
		}
		
		
	}
}

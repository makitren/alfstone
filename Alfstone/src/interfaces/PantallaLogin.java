package interfaces;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextPane;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import baseDeDatos.BD;
import clases.Usuario;
import clases.Ventanas;

import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PantallaLogin extends JPanel {
	private JTextField txtUsu;
	private JTextField txtContra;
	private JTextField textoEscritoUsu;
	private JPasswordField textoEscritoContra;
	private Ventanas ventana;
	private Usuario usuario;
	private Statement executeStatement;
	private Connection bd;
	
	public PantallaLogin(Ventanas v) {
		super();
		//Crea la conexion con la base de datos
		try {
			bd=DriverManager.getConnection(BD.bdNombre,BD.bdUsuario,BD.bdContrase�a);
			executeStatement=bd.createStatement();
		}catch(SQLException e) {
			JOptionPane.showMessageDialog(ventana, "No se ha podido iniciar la conexion con la base de datos");
			e.printStackTrace();
		}
		this.ventana=v;
		this.setSize(1200,1000);
		 setLayout(null);
		
		JTextPane txtJugado = new JTextPane();
		txtJugado.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 30));
		txtJugado.setForeground(new Color(255, 255, 0));
		txtJugado.setOpaque(false);
		txtJugado.setText("\u00BFYa has jugado antes? \u00BFAcaso eres masoca?");
		txtJugado.setBounds(205, 13, 767, 52);
		 add(txtJugado);
		
		txtUsu = new JTextField();
		txtUsu.setForeground(new Color(255, 255, 0));
		txtUsu.setEditable(false);
		txtUsu.setHorizontalAlignment(SwingConstants.CENTER);
		txtUsu.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 30));
		txtUsu.setText("Alias");
		txtUsu.setOpaque(false);
		txtUsu.setBounds(163, 204, 236, 69);
		 add(txtUsu);
		txtUsu.setColumns(10);
		 
		txtContra = new JTextField();
		txtContra.setEditable(false);
		txtContra.setForeground(new Color(255, 255, 0));
		txtContra.setHorizontalAlignment(SwingConstants.CENTER);
		txtContra.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 30));
		txtContra.setText("Contrase\u00F1a");
		txtContra.setOpaque(false);
		txtContra.setBounds(163, 380, 236, 69);
		 add(txtContra);
		txtContra.setColumns(10);
		//Aqui se introduce el alias del usuario
		textoEscritoUsu = new JTextField();
		textoEscritoUsu.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 21));
		textoEscritoUsu.setBounds(637, 229, 277, 44);
		 add(textoEscritoUsu);
		textoEscritoUsu.setColumns(10);
		
		//Aqui se introduce la contrase�a
		textoEscritoContra = new JPasswordField();
		textoEscritoContra.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 21));
		textoEscritoContra.setBounds(637, 404, 277, 44);
		 add(textoEscritoContra);
		textoEscritoContra.setColumns(10);
	String.valueOf(textoEscritoContra.getPassword());
		//Boton que devuelve a la pantalla inicial
		JButton btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ventana.cargarPantallaInicio();
			}
		});
		btnVolver.setFont(new Font("Tahoma", Font.PLAIN, 21));
		btnVolver.setBounds(205, 691, 141, 52);
		 add(btnVolver);
		//Este boton se encarga de ejecutar la funcion de iniciar sesion
		JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(iniciarSesion()!=null) {
					ventana.setUsuarioLog(iniciarSesion());
					ventana.cargarPantallaLobby();
				}else {
					JOptionPane.showMessageDialog(ventana, "Error al introducir el usuario o la contrase�a");
				}
			}
		});
		btnIniciar.setFont(new Font("Tahoma", Font.PLAIN, 21));
		btnIniciar.setBounds(656, 691, 166, 52);
		 add(btnIniciar);
		 
		
	
		
		BufferedImage fondoPantalla=null;
		   setLayout(null);
		JPanel panel=new JPanel();
		try {
			fondoPantalla=ImageIO.read(new File("./src/imagenesInterfaces/ImagenFondoPantallaLogin.png"));
		}catch (IOException e) {
			e.printStackTrace();
			System.err.println("No se puede leer la imagen");
		}
		JLabel fondo=new JLabel(new ImageIcon(fondoPantalla));
		panel.setBounds(0,0,this.getWidth(),this.getHeight());
		panel.setLayout(null);
		panel.add(fondo);
		fondo.setBounds(0,0,this.getWidth(),this.getHeight());
		   add(panel);
		this.setVisible(true);
		
	}
	//Funcion que busca el usuario en la base de datos. Si lo encuentra, permite acceder al juego, si no salta error
	public Usuario iniciarSesion() {
		try {
			
			ResultSet rs= executeStatement.executeQuery("SELECT * "
					+ "FROM usuario"
					+ " WHERE alias='"+textoEscritoUsu.getText()+"' AND contrasenia='"+String.valueOf(textoEscritoContra.getPassword())+"'");
			if(rs.next()) {
				Usuario usuarioIniciado=new Usuario(rs.getString("nombre"),rs.getString("alias"),rs.getString("contrasenia"),rs.getString("email"));
				return usuarioIniciado;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}

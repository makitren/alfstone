package interfaces;

import javax.swing.JPanel;

import clases.Carta;
import clases.Enemigo;
import clases.Ventanas;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PantallaJefes extends JPanel{
	private Ventanas ventana;
	private HashMap<String,Carta> mano;
	public PantallaJefes(Ventanas v) {
		this.ventana=v;
		
		this.setSize(1200,1000);
		setLayout(null);
		
		JPanel panelNormal = new JPanel();
		panelNormal.setBounds(12, 127, 384, 536);
		add(panelNormal);
		panelNormal.setLayout(null);
		panelNormal.setOpaque(false);
		
		JLabel lblNormal = new JLabel();
		lblNormal.setBounds(-24, -19, 396, 555);
		panelNormal.add(lblNormal);
		//Imagen del jefe normal
		lblNormal.setIcon(new ImageIcon("./src/imagenesJefes/JefeNormalBueno.png"));
		
		JPanel panelProvocar = new JPanel();
		panelProvocar.setBounds(408, 127, 400, 536);
		add(panelProvocar);
		panelProvocar.setLayout(null);
		
		JLabel lblProvocar = new JLabel();
		lblProvocar.setBounds(-29, 0, 417, 538);
		panelProvocar.add(lblProvocar);
		panelProvocar.setOpaque(false);
		//Imagen del jefe sigilo
		lblProvocar.setIcon(new ImageIcon("./src/imagenesJefes/JefeSigiloBueno.png"));
		
		JLabel lblSigilo = new JLabel();
		lblSigilo.setBounds(793, 127, 378, 536);
		add(lblSigilo);
		
		JPanel panelSigilo = new JPanel();
		panelSigilo.setBounds(793, 142, 378, 521);
		add(panelSigilo);
		panelSigilo.setLayout(null);
		panelSigilo.setOpaque(false);
		//Imagen del jefe provocar
		lblSigilo.setIcon(new ImageIcon("./src/imagenesJefes/JefeProvocarBueno.png"));
		//Boton el cual lleva a un tablero con las especificaciones del Jefe Normal
		JButton btnJefeNormal = new JButton("Luchar !");
		btnJefeNormal.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Enemigo jefeNormal=new Enemigo("JefeNormal",ventana);
				ventana.setEnemigo(jefeNormal);
				ventana.irATablero();
			}
		});
		btnJefeNormal.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 21));
		btnJefeNormal.setBounds(133, 688, 142, 47);
		add(btnJefeNormal);
		//Boton el cual lleva a un tablero con las especificaciones del Jefe Sigilo
		JButton btnJefeSigilo = new JButton("Luchar !");
		btnJefeSigilo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Enemigo jefeSigilo=new Enemigo("JefeSigilo",ventana);
				ventana.setEnemigo(jefeSigilo);
				ventana.irATablero();
			}
		});
		btnJefeSigilo.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 21));
		btnJefeSigilo.setBounds(508, 688, 148, 47);
		add(btnJefeSigilo);
		
		//Boton el cual lleva a un tablero con las especificaciones del Jefe Provocar
		JButton btnJefeProvocar = new JButton("Luchar !");
		btnJefeProvocar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Enemigo jefeProvocar=new Enemigo("JefeProvocar",ventana);
				ventana.setEnemigo(jefeProvocar);
				ventana.irATablero();
			}
		});
		btnJefeProvocar.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 21));
		btnJefeProvocar.setBounds(891, 688, 167, 47);
		add(btnJefeProvocar);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ventana.cargarPantallaLobby();
			}
		});
		btnVolver.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 21));
		btnVolver.setBounds(525, 844, 131, 47);
		add(btnVolver);
		
		
		//Fondo de pantalla
		BufferedImage fondoPantalla=null;
		 setLayout(null);
		JPanel panel=new JPanel();
		try {
			fondoPantalla=ImageIO.read(new File("./src/imagenesInterfaces/FondoPantallaJefes.png"));
		}catch (IOException e) {
			e.printStackTrace();
			System.err.println("No se puede leer la imagen");
		}
		JLabel fondo=new JLabel(new ImageIcon(fondoPantalla));
		panel.setBounds(0,13,this.getWidth(),this.getHeight());
		panel.add(fondo);
		fondo.setBounds(0,0,this.getWidth(),this.getHeight());
		 add(panel);
		this.setVisible(true);
	}
}

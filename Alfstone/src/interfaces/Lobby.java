package interfaces;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import clases.Ventanas;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Lobby extends JPanel{
	private Ventanas ventana;
	
	public Lobby(Ventanas v) {
	this.ventana=v;
	this.setBounds(0, 0, 1200, 1000);
	setLayout(null);
	Scanner s=new Scanner(System.in);
	//Image que hace de boton y lleva a la creacion del mazo
	JLabel labelCreacion = new JLabel();
	labelCreacion.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			ventana.cargarPantallaCreacionMazo();
		}
	});
	labelCreacion.setBounds(637, 270, 290, 225);
	add(labelCreacion);
	
	JPanel panelCreacion = new JPanel();
	panelCreacion.setBounds(637, 270, 290, 225);
	add(panelCreacion);
	panelCreacion.setLayout(null);
	/*
	 * labelCreacion tiene panelCreacion para poder usar la imagen como un boton
	 * Lo mismo con los demas paneles
	 */
	labelCreacion.setIcon(new ImageIcon("./src/imagenesIconos/IconoCreacionBueno.png"));
	//labelPelear que usa la imagen como Boton para llevar ante los Jefes
	JLabel labelPelear = new JLabel();
	labelPelear.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			ventana.cargarPantallaJefes();
		}
	});
	labelPelear.setBounds(124, 270, 290, 225);
	add(labelPelear);
	
	JPanel panelPelear = new JPanel();
	panelPelear.setBounds(124, 270, 290, 225);
	add(panelPelear);
	panelPelear.setLayout(null);
	
	labelPelear.setIcon(new ImageIcon("./src/imagenesIconos/IconoPelearBueno.png"));
	
	JLabel labelDatos = new JLabel();
	labelDatos.setBounds(126, 618, 290, 225);
	add(labelDatos);
	
	JPanel panelDatos  = new JPanel();
	panelDatos .setBounds(126, 618, 290, 225);
	add(panelDatos );
	panelDatos .setLayout(null);
	
	//labelDatos crea un fichero de texto en el que se recoge la informacion del usuario, el email y el nombre del jugador
	labelDatos.setIcon(new ImageIcon("./src/imagenesIconos/IconoInformacionBueno.png"));
	FileReader ficheroLeer=null;
	BufferedReader lector=null;
	labelDatos.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			FileWriter ficheroEscribir = null;
			try {
				for(int i=0;i<1000;i++) {
					ficheroEscribir = new FileWriter("Partida"+i+".txt");
				}
					// Escribimos linea a linea en el fichero
					
						ficheroEscribir.write("Usuario: "+ventana.getUsuarioLog().getAlias()+"\n"+
											  "\nEmail: "+ventana.getUsuarioLog().getEmail()+"\n"+
											  "\nNombre: "+ventana.getUsuarioLog().getNombre());
						ficheroEscribir.flush();
					
					ficheroEscribir.close();

				} catch (Exception ex) {
					System.out.println("Mensaje de la excepci�n: " + ex.getMessage());
				}

			
		}
	});
	//labelInicio usa la imagen para llevar de vuelta al inicio
	JLabel labelInicio = new JLabel();
	labelInicio.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			ventana.cargarPantallaInicio();
		}
	});
	labelInicio.setBounds(637, 618, 290, 225);
	add(labelInicio);
	
	JPanel panelInicio  = new JPanel();
	panelInicio .setBounds(637, 618, 290, 225);
	add(panelInicio );
	panelInicio .setLayout(null);
	
	labelInicio.setIcon(new ImageIcon("./src/imagenesIconos/IconoInicioBueno.png"));
	
	JLabel fondo=new JLabel();
	fondo.setIcon(new ImageIcon(CreacionMazo.class.getResource("/imagenesInterfaces/FondoLobbyBueno.jpg")));
	fondo.setBounds(0,0,1200,1000);
	add(fondo);

	
		
	}
}
